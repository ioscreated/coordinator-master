import Foundation
import UIKit

final class AppCoordinator: BaseCoordinator {
    
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() {
        self.chooseFlow()
    }
    
    private func chooseFlow() {
        if self.isAuthorized() {
            self.startMain()
        }
        else {
            self.startLogin()
        }
    }
    
    private func startLogin() {
        let nc = UINavigationController()
        let loginCoordinator = LoginCoordinator(router: nc)
        addDependency(loginCoordinator)
        loginCoordinator.completionHandler = { [unowned self] in
            self.startMain()
            self.removeDependency(loginCoordinator)
        }
        
        window.changeRootViewController(nc)
        loginCoordinator.start()
    }
    
    private func startMain() {
        let tb = UITabBarController()
        let mainCoordinator = MainCoordinator(router: tb)
        addDependency(mainCoordinator)
        mainCoordinator.completionHandler = {
            self.startLogin()
            self.removeDependency(mainCoordinator)
        }
        window.changeRootViewController(tb)
        mainCoordinator.start()
    }
}

extension AppCoordinator {
    func isAuthorized() -> Bool {
        return false
    }
}
