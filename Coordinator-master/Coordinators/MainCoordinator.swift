import Foundation
import UIKit

final class MainCoordinator: BaseCoordinator {
    let router: UITabBarController
    
    init(router: UITabBarController){
        self.router = router
    }
    
    override func start() {
        self.router.viewControllers = [getSomeFlow(),
                                       getSomeFlow(),
                                       getSomeFlow()]
    }
    
    private func getSomeFlow() -> UINavigationController {
        let flowNC = UINavigationController()
        let someCoordinator = NewsCoordinator.init(router: flowNC)
        addDependency(someCoordinator)
        someCoordinator.start()
        flowNC.tabBarItem.image = #imageLiteral(resourceName: "TabBarIcon")
        flowNC.tabBarItem.selectedImage = #imageLiteral(resourceName: "TabBarIcon")
        return flowNC
    }
}
