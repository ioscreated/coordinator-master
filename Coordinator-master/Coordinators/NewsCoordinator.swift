import Foundation
import UIKit

final class NewsCoordinator: BaseCoordinator {
    let router: UINavigationController
    
    init(router: UINavigationController) {
        self.router = router
    }
    
    override func start() {
        self.startNewsList()
    }
    
    private func startNewsList() {
        let vc = NewsListVC()
        self.router.pushViewController(vc, animated: true)
    }
}
