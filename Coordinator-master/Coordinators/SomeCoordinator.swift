import Foundation
import UIKit

final class SomeCoordinator: BaseCoordinator {
    let router: UINavigationController
    
    init(router: UINavigationController) {
        self.router = router
    }
    
    override func start() {
        self.startSomeScreen()
    }
    
    private func startSomeScreen() {
        let vc = PlaceholderController()
        self.router.pushViewController(vc, animated: true)
    }
}
