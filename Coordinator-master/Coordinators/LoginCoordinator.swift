import Foundation
import UIKit

final class LoginCoordinator: BaseCoordinator {
    
    let router: UINavigationController
    
    init(router: UINavigationController) {
        self.router = router
    }
    
    override func start() {
        self.startLogin()
    }
    
    private func startLogin() {
        let vc = LoginController()
        vc.loginCompletion = { [weak self] in
            guard let self = self else {return}
            self.completionHandler?()
        }
        self.router.pushViewController(vc, animated: true)
    }
}


extension LoginCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController,
                              animated: Bool) {
        guard let fromVC = navigationController.transitionCoordinator?.viewController(forKey: .from) else { return }
        if navigationController.viewControllers.contains(fromVC) { return }
        
        if fromVC is LoginController {
            self.completionHandler?()
        }
    }
}
