import Foundation
import UIKit
import SnapKit

class LoginController: UIViewController {
    
    var loginCompletion: (() -> ())?
    
    private let inputLogin: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Логин"
        return textField
    }()
    
    private let inputPass: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Пароль"
        return textField
    }()
    
    private let confirmButton: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(.white, for: .normal)
        btn.setTitle("ПОДТВЕРДИТЬ", for: .normal)
        btn.backgroundColor = .blue
        return btn
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.fill
        stackView.spacing = 16
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.view.backgroundColor = .white
    }
    
    private func setupUI() {
        self.view.addSubview(self.stackView)
        self.stackView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }
        self.stackView.addArrangedSubview(self.inputLogin)
        self.stackView.addArrangedSubview(self.inputPass)
        self.stackView.addArrangedSubview(self.confirmButton)
        
        self.confirmButton.addTarget(self, action: #selector(confirmBtnTapped), for: .touchUpInside)
    }
    
    @objc private func confirmBtnTapped() {
        guard let login = self.inputLogin.text, let pass = self.inputPass.text, login.count > 0 && pass.count > 0 else {return}
        self.loginCompletion?()
    }
}
