import Foundation
import UIKit
import SnapKit

class NewsCell: UITableViewCell {
    private let logotypeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .black
        label.alpha = 0.7
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .black
        return label
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing = 4
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.logotypeImageView.layer.cornerRadius = self.logotypeImageView.frame.height/2
    }
    
    private func initialize() {
        self.addSubview(self.logotypeImageView)
        self.logotypeImageView.layer.masksToBounds = true
        self.logotypeImageView.snp.makeConstraints { (make) in
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(8)
            make.bottom.greaterThanOrEqualToSuperview().offset(-8).priority(.high)
            make.top.greaterThanOrEqualToSuperview().offset(8).priority(.high)
        }
        self.logotypeImageView.backgroundColor = .lightGray
        
        self.addSubview(self.stackView)
        self.stackView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(8)
            make.left.equalTo(self.logotypeImageView.snp.right).inset(-8)
            make.bottom.equalToSuperview().offset(-8)
            make.top.equalToSuperview().offset(8)
           // make.bottom.greaterThanOrEqualToSuperview().offset(-8).priority(.high)
           // make.top.greaterThanOrEqualToSuperview().offset(8).priority(.high)
        }
        
        self.stackView.addArrangedSubview(self.titleLabel)
        self.stackView.addArrangedSubview(self.descriptionLabel)
    }
    
    public func setNews(news: NewsItem) {
        self.titleLabel.text = news.headerTitle
        self.descriptionLabel.text = news.text
    }
}
