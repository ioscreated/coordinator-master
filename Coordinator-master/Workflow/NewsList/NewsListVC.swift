import Foundation
import UIKit

class NewsListVC: UIViewController {
    
    private var news: [NewsItem] = []
    
    private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundColor = .white
        tableView.register(NewsCell.self, forCellReuseIdentifier: NewsCell.nameOfClass)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.news = self.generateNews(count: 20)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    private func generateNews(count: Int) -> [NewsItem] {
        var items: [NewsItem] = []
        for _ in 0 ..< count {
            let title = String.randomString(length: 12)
            let text = String.randomText(length: Int.random(in: 30 ..< 160), maxWordLength: 12)
            let news = NewsItem.init(headerTitle: title, text: text, logotypeImageURL: nil)
            items.append(news)
        }
        return items
    }
    
    private func configureCell(cell: NewsCell, item: NewsItem) {
        cell.setNews(news: item)
    }
}


extension NewsListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsCell.nameOfClass, for: indexPath) as! NewsCell
        self.configureCell(cell: cell, item: self.news[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension NewsListVC: UITableViewDelegate {
    
}
