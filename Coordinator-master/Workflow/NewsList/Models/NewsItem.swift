import Foundation

struct NewsItem {
    let headerTitle: String
    let text: String
    let logotypeImageURL: URL?
}
