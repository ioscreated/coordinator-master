import Foundation

class BaseCoordinator: NSObject, Coordinator {
    
    var completionHandler: CoordinatorCompletionHandler = nil
    
    var childCoordinators: [Coordinator] = []
    
    func start() {
        print("test")
        print("another test")
    }
    
    func addDependency(_ coordinator: Coordinator) {
        for element in childCoordinators {
            if element === coordinator { return }
        }
        childCoordinators.append(coordinator)
    }
    
    func removeDependency(_ coordinator: Coordinator) {
        guard childCoordinators.isEmpty == false else { return }
        
        for (index, element) in childCoordinators.enumerated() {
            if element === coordinator {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    
   
    
}
