import Foundation

protocol Coordinator: class {
    typealias CoordinatorCompletionHandler = (() -> ())?
    
    var childCoordinators: [Coordinator] { get set }
    var completionHandler: CoordinatorCompletionHandler {get set}

    func start()
    func addDependency(_ coordinator: Coordinator)
    func removeDependency(_ coordinator: Coordinator)
}
