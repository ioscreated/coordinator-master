extension String {
    static func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
  static func randomText(length: Int, maxWordLength: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    var resultText: String = ""
    var randomWordLength: Int = Int.random(in: 1 ..< maxWordLength)
    for i in 0 ..< length {
        if i != randomWordLength {
            resultText.append(letters.randomElement()!)
        }
        else {
            resultText.append(" ")
            randomWordLength = Int.random(in: i + 1 ..< i + 1 + maxWordLength)
        }
    }
    return resultText
    }
}
